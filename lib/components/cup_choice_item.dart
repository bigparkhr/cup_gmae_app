import 'package:flutter/material.dart';

class CupChoiceItem extends StatefulWidget {
  const CupChoiceItem({
    super.key,
    required this.pandon,
    required this.currentMoney
  });

  final num pandon; // 100
  final num currentMoney; // 0

  @override
  State<CupChoiceItem> createState() => _CupChoiceItemState();
}

class _CupChoiceItemState extends State<CupChoiceItem> {
  bool _isOpen = false;
  String imgSrc = 'cup11.jpeg';

  void _calculateStart() {
    if (!_isOpen) {
      setState(() {
        _isOpen = true;
      });
    }
  }

  void _calculateImgSrc() {
    String tempImgSrc = '';
    if (_isOpen) {
      if (widget.pandon < widget.currentMoney) {
        tempImgSrc = 'assets/dam.jpg';
      } else if (widget.pandon == widget.currentMoney) {
        tempImgSrc = 'assets/coin1,jpg';
      } else {
        tempImgSrc = 'assets/coin2.jpg';
      }
    } else {
      tempImgSrc = 'assets/cup11.jpeg';
    }

    setState(() {
      imgSrc = tempImgSrc;
    });
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        _calculateStart();
        _calculateImgSrc();
      },
      child: SizedBox(
        width: 100,
        height: 100,
        child: Image.asset(imgSrc),
      )
    );
  }
}
